package Aufgabe_1_Graphentheorie;

import Aufgabe_1_Graphentheorie.dijkstra.clsDijkstraManager;
import Aufgabe_1_Graphentheorie.graph.clsGraph;
import Aufgabe_1_Graphentheorie.graph.clsVertex;

/**
 * @author Tom Alexander Schmanke
 * @matriculationnumber 9401801
 * @email s191850@student.dhbw-mannheim.de
 *
 * Main class where the objects are initialized
 *
 * Methods:
 *  graph.addVertex(T label)
 *  graph.deleteVertex(T label)
 *  graph.addDirectedEdge(T startLabel, T destinationLabel, double weight)
 *  graph.addUndirectedEdge(T label1, T label2, double weight)
 *  graph.deleteEdge(T startLabel, T destinationLabel, double weight)
 *  graph.printGraph()
 *  graph.getEdges(clsVertex<T> vertex)
 *  graph.getReachableVertexes(clsVertex<T> startVertex)
 *
 *  dijkstraManager.dijkstraAlgorithm(clsGraph<T> graph, clsVertex<T> startingVertex, clsVertex<T> endingVertex, boolean debugPrinting)
 *
 *  Nearer descriptions can be found as JavaDocs by the methods
 */
public class clsMain {

    public static void main (String[] args){

        clsGraph<String> graph = new clsGraph<>();
        clsDijkstraManager<String> dijkstraManager = new clsDijkstraManager<>();

        graph.addVertex("Frankfurt");
        graph.addVertex("Mannheim");
        graph.addVertex("Wuerzburg");
        graph.addVertex("Stuttgard");
        graph.addVertex("Karlsruhe");
        graph.addVertex("Erfurt");
        graph.addVertex("Nuernberg");
        graph.addVertex("Kassel");
        graph.addVertex("Augsburg");
        graph.addVertex("Muenchen");

        graph.addUndirectedEdge("Frankfurt", "Mannheim", 85);
        graph.addUndirectedEdge("Frankfurt", "Wuerzburg", 217);
        graph.addUndirectedEdge("Frankfurt", "Kassel", 173);
        graph.addUndirectedEdge("Mannheim", "Karlsruhe", 80);
        graph.addUndirectedEdge("Wuerzburg", "Erfurt", 186);
        graph.addUndirectedEdge("Wuerzburg", "Nuernberg", 103);
        graph.addUndirectedEdge("Stuttgard", "Nuernberg", 183);
        graph.addUndirectedEdge("Karlsruhe", "Augsburg", 250);
        graph.addUndirectedEdge("Nuernberg", "Muenchen", 167);
        graph.addUndirectedEdge("Kassel", "Muenchen", 502);
        graph.addUndirectedEdge("Augsburg", "Muenchen", 84);

        graph.printGraph();

        dijkstraManager.dijkstraAlgorithm(graph, new clsVertex<>("Frankfurt"), new clsVertex<>("Muenchen"), true);

    }

}
