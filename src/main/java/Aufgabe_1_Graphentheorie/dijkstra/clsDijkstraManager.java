package Aufgabe_1_Graphentheorie.dijkstra;

import Aufgabe_1_Graphentheorie.graph.clsGraph;
import Aufgabe_1_Graphentheorie.graph.clsVertex;

import java.util.*;

public class clsDijkstraManager <T> {

    /**
     * Calls the dijkstraAlgorithm method with a default false parameter for debugPrinting
     * @param graph A normal Graph
     * @param startingVertex The vertex from where the path should start
     * @param endingVertex The vertex where the path should end
     */
    public void dijkstraAlgorithm(clsGraph<T> graph, clsVertex<T> startingVertex, clsVertex<T> endingVertex){
        dijkstraAlgorithm(graph, startingVertex, endingVertex, false);
    }

    /**
     * Implementation of the Dijkstra algorithm. The minimalDistance from every vertex to the start vertex gets calculated
     * and then the path from the start to the end vertex gets computed
     * @param graph A normal Graph
     * @param startingVertex The vertex from where the path should start
     * @param endingVertex The vertex where the path should end
     * @param debugPrinting If true the minimum distance to all vertexes are printed
     */
    public void dijkstraAlgorithm(clsGraph<T> graph, clsVertex<T> startingVertex, clsVertex<T> endingVertex, boolean debugPrinting){

        Map<clsVertex<T>, clsWrapper<T>> unvisitedVertexes = new HashMap<>(); //HashMap containing all founded but not visited vertexes
        unvisitedVertexes.put(startingVertex, new clsWrapper<>(graph.getEdges(startingVertex), 0d, null)); //Adds the start vertex with the minimalDistance of zero and no predecessor to the Map of the unvisited vertexes

        Map<clsVertex<T>, clsWrapper<T>> visitedVertexes = new HashMap<>(); //HashMap containing all already visited vertexes

        clsVertex<T> currentVertex = startingVertex; //Set the starting vertex to the current vertex

        while(unvisitedVertexes.size() != 0){
            visitedVertexes.put(currentVertex, unvisitedVertexes.remove(currentVertex)); //Removes the current vertex from the unvisited HashMap and add it to the visited HashMap
            Map<clsVertex<T>, clsWrapper<T>> reachableVertexes = graph.getReachableVertexes(currentVertex); //Computes all the vertexes which can be accessed by the current vertex in one step
            unvisitedVertexes = updateUnvisitedVertexes(reachableVertexes, unvisitedVertexes, visitedVertexes, visitedVertexes.get(currentVertex).getMinimalDistance(), currentVertex); //Add the reachable Vertexes in the unvisited list if necessary and computes their new minimalDistance value

            if(!unvisitedVertexes.isEmpty()) { //Prevents error in the last iteration when there are no more unvisited elements
                currentVertex = newNearestVertex(unvisitedVertexes); //Sets the new vertex with the smallest minimalDistance to the current vertex
            }
        }

        printResults(debugPrinting, visitedVertexes, endingVertex); //Prints the results to the console

    }

    /**
     * Computes the clsVertex with the smallest minimalDistance value
     * @param vertexes Map of clsVertexes and clsWrapper
     * @return The clsVertex with the smallest corresponding minimalDistance value
     */
    private clsVertex<T> newNearestVertex(Map<clsVertex<T>, clsWrapper<T>> vertexes) {
        Optional<Map.Entry<clsVertex<T>, clsWrapper<T>>> minValue = vertexes.entrySet().stream().min(Comparator.comparingDouble(o -> o.getValue().getMinimalDistance()));
        return minValue.get().getKey();
    }

    /**
     * Adds the new vertexes to the list of unvisited vertexes if they are not in the list of visited vertexes or are already in the list with a smaller minimalDistance value.
     * During the adding the new minimalDistance is computed (minimalDistance from the current vertex + minimalDistance from the vertex which should be added)
     * @param newVertexes The map of vertexes which should be computed
     * @param unvisitedVertexes The current map of unvisited vertexes
     * @param visitedVertexes The map of visited vertexes
     * @param startWeight The minimalDistance value from the current vertex
     * @param currentVertex The current vertex
     * @return The new map of unvisited vertexes
     */
    private Map<clsVertex<T>, clsWrapper<T>> updateUnvisitedVertexes(Map<clsVertex<T>, clsWrapper<T>> newVertexes, Map<clsVertex<T>, clsWrapper<T>> unvisitedVertexes, Map<clsVertex<T>, clsWrapper<T>> visitedVertexes, double startWeight, clsVertex<T> currentVertex){
        newVertexes.forEach((key, value) -> {
            if(!(visitedVertexes.containsKey(key) || (
                    unvisitedVertexes.containsKey(key) &&
                            unvisitedVertexes.get(key).getMinimalDistance() < value.getMinimalDistance() + startWeight
            ))){
                unvisitedVertexes.put(key, new clsWrapper<>(value.getEdges(), value.getMinimalDistance() + startWeight, currentVertex));
            }
        });
        return unvisitedVertexes;
    }

    /**
     * Computes the shortest path to a vertex out of an graph computed by the Dijkstra algorithm
     * @param dijkstraGraph The graph which was computed by the Dijkstra algorithm
     * @param endVertex The destination vertex
     * @return A stack containing the ordered vertexes. The first vertex is at the top and the destination vertex at the bottom
     */
    private Stack<clsVertex<T>> computePath(Map<clsVertex<T>, clsWrapper<T>> dijkstraGraph, clsVertex<T> endVertex){
        Stack<clsVertex<T>> computedPath = new Stack<>();
        computedPath.push(endVertex);

        while(dijkstraGraph.get(computedPath.peek()).getPredecessor() != null){
            computedPath.push(dijkstraGraph.get(computedPath.peek()).getPredecessor());
        }

        return computedPath;
    }

    /**
     * Prints the results of the Dijkstra algorithm to the console
     * @param debugPrinting If true the minimum distance to all vertexes are printed
     * @param dijkstraGraph The graph which was computed by the Dijkstra algorithm
     * @param endingVertex The destination vertex
     */
    private void printResults(boolean debugPrinting, Map<clsVertex<T>, clsWrapper<T>> dijkstraGraph, clsVertex<T> endingVertex){
        if(debugPrinting){
            dijkstraGraph.forEach((key, value) -> {
                System.out.println("Vertex: " + key.toString());
                System.out.println("Minimal Distance: " + value.getMinimalDistance());
                System.out.println("Predecessor: " + value.getPredecessor());
                System.out.println();
            });
        }
        Stack<clsVertex<T>> computedPath = computePath(dijkstraGraph, endingVertex);
        while(!computedPath.isEmpty()){
            System.out.println(computedPath.pop());
        }
        System.out.println("Distance: " + dijkstraGraph.get(endingVertex).getMinimalDistance());
    }
}

