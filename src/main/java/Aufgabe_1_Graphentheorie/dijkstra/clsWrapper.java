package Aufgabe_1_Graphentheorie.dijkstra;

import Aufgabe_1_Graphentheorie.graph.clsVertex;
import Aufgabe_1_Graphentheorie.graph.edges.IEdge;

import java.util.ArrayList;

/**
 * @author Tom Alexander Schmanke
 * @matriculationnumber 9401801
 * @email s191850@student.dhbw-mannheim.de
 *
 * Wrapper class containing the edges from a vertex, a repository for the minimal distance
 * which is needed to compute the shortest way between to vertexes with the Dijkstra algorithm and the predecessor in the shortest path
 * @param <T> The general type of the label of the vertexes
 */
public class clsWrapper<T> {

    private final ArrayList<IEdge<T>> edges;
    private final double minimalDistance;
    private final clsVertex<T> predecessor;

    /**
     * Constructor initially setting the values of the variables
     * @param edges A list of the edges from the vertex
     * @param minimalDistance The minimal distance to the start vertex. Gets computed during the algorithm
     * @param predecessor The predecessor vertex to recapitulate the path you have to go, when you want to take the shortest way
     */
    public clsWrapper(ArrayList<IEdge<T>> edges, double minimalDistance, clsVertex<T> predecessor){
        this.edges = edges;
        this.minimalDistance = minimalDistance;
        this.predecessor = predecessor;
    }

    /**
     * Getter method for minimalDistance
     * @return The value of minimalDistance
     */
    public double getMinimalDistance(){
        return minimalDistance;
    }

    /**
     * Getter method for edges
     * @return The value of edges
     */
    public ArrayList<IEdge<T>> getEdges(){
        return edges;
    }

    /**
     * Getter method for predecessor
     * @return The value of predecessor
     */
    public clsVertex<T> getPredecessor(){
        return predecessor;
    }

}
