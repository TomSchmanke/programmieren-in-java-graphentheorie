package Aufgabe_1_Graphentheorie.graph.edges;

import Aufgabe_1_Graphentheorie.graph.clsVertex;

/**
 * @author Tom Alexander Schmanke
 * @matriculationnumber 9401801
 * @email s191850@student.dhbw-mannheim.de
 *
 * Interface for the two different implementations of clsEdge (clsEdgeDirected, clsEdgeUndirected)
 * @param <T> The general type of the label of the vertexes
 */
public interface IEdge<T> {

    clsVertex<T> getDestinationPoint();

    double getWeight();

}
