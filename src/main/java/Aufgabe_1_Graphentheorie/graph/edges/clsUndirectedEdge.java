package Aufgabe_1_Graphentheorie.graph.edges;

import Aufgabe_1_Graphentheorie.graph.clsVertex;

import java.util.Objects;

/**
 * @author Tom Alexander Schmanke
 * @matriculationnumber 9401801
 * @email s191850@student.dhbw-mannheim.de
 *
 * Implementation of the undirected edge of a graph
 * @param <T> The general type of the label of the vertexes
 */
public class clsUndirectedEdge<T> implements IEdge<T> {

    private final clsVertex<T> destinationPoint;
    private final double weight;

    /**
     * Constructor initially setting the values of the variables
     * @param destinationPoint The vertex the edge go to
     * @param weight The weight of the edge
     */
    public clsUndirectedEdge(clsVertex<T> destinationPoint, double weight){
        this.destinationPoint = destinationPoint;
        this.weight = weight;
    }

    /**
     * Getter method for destinationPoint
     * @return The value of destinationPoint
     */
    @Override
    public clsVertex<T> getDestinationPoint() {
        return destinationPoint;
    }

    /**
     * Getter method for weight
     * @return The value of weight
     */
    @Override
    public double getWeight(){
        return weight;
    }

    /**
     * Pretty printing for a better output of the graph
     * @return A string containing the conditioned values of the object
     */
    @Override
    public String toString() {
        return "Edge (" + "destinationPoint: " + destinationPoint + ", weight: " + weight + ')';
    }

    /**
     * Basic equals method
     * @param o The object which should be compared to the this object
     * @return True if the elements are the same or the weight and the destinationPoint are the same, false instead
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof clsUndirectedEdge)) return false;
        clsUndirectedEdge<?> that = (clsUndirectedEdge<?>) o;
        return Double.compare(that.weight, weight) == 0 &&
                Objects.equals(destinationPoint, that.destinationPoint);
    }

    /**
     * Basic hash code method
     * @return Hash code
     */
    @Override
    public int hashCode() {
        return Objects.hash(destinationPoint, weight);
    }

}
