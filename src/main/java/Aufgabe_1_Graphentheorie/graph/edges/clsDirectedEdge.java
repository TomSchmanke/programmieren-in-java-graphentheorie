package Aufgabe_1_Graphentheorie.graph.edges;

import Aufgabe_1_Graphentheorie.graph.clsVertex;

/**
 * @author Tom Alexander Schmanke
 * @matriculationnumber 9401801
 * @email s191850@student.dhbw-mannheim.de
 *
 * Implementation of the directed edge of a graph
 * @param <T> The general type of the label of the vertexes
 */
public class clsDirectedEdge<T> implements IEdge<T> {

    private final clsVertex<T> destinationPoint;
    private final double weight;

    /**
     * Constructor initially setting the values of the variables
     * @param destinationPoint The vertex the edge go to
     * @param weight The weight of the edge
     */
    public clsDirectedEdge(clsVertex<T> destinationPoint, double weight){
        this.destinationPoint = destinationPoint;
        this.weight = weight;
    }

    /**
     * Getter method for destinationPoint
     * @return The value of destinationPoint
     */
    @Override
    public clsVertex<T> getDestinationPoint() {
        return destinationPoint;
    }

    /**
     * Getter method for weight
     * @return The value of weight
     */
    @Override
    public double getWeight(){
        return weight;
    }

    /**
     * Pretty printing for a better output of the graph
     * @return A string containing the conditioned values of the object
     */
    @Override
    public String toString() {
        return "Edge (" + "destinationPoint: " + destinationPoint + ", weight: " + weight + ')';
    }

}
