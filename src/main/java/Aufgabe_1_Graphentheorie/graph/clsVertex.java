package Aufgabe_1_Graphentheorie.graph;

import java.util.Objects;

/**
 * @author Tom Alexander Schmanke
 * @matriculationnumber 9401801
 * @email s191850@student.dhbw-mannheim.de
 *
 * Implementation of the vertex
 * @param <T> The general type of the label of the vertexes
 */
public class clsVertex<T> {

    private final T label;

    /**
     * Constructor initially setting the values of the variables
     * @param label The "name" of the vertex
     */
    public clsVertex(T label){
        this.label = label;
    }

    /**
     * Getter method for label
     * @return The value of label
     */
    public T getLabel(){
        return label;
    }

    /**
     * Pretty printing for a better output of the graph
     * @return A string containing the conditioned values of the object
     */
    @Override
    public String toString() {
        return "Vertex: " + label;
    }

    /**
     * Basic equals method
     * @param o The object which should be compared to the this object
     * @return True if the elements are the same or the labels are the same, false instead
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof clsVertex)) return false;
        clsVertex<?> clsVertex = (clsVertex<?>) o;
        return Objects.equals(label, clsVertex.label);
    }

    /**
     * Basic hash code method
     * @return Hash code
     */
    @Override
    public int hashCode() {
        return Objects.hash(label);
    }

}
