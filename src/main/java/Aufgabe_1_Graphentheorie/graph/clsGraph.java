package Aufgabe_1_Graphentheorie.graph;

import Aufgabe_1_Graphentheorie.dijkstra.clsWrapper;
import Aufgabe_1_Graphentheorie.graph.edges.IEdge;
import Aufgabe_1_Graphentheorie.graph.edges.clsDirectedEdge;
import Aufgabe_1_Graphentheorie.graph.edges.clsUndirectedEdge;

import java.util.*;

/**
 * @author Tom Alexander Schmanke
 * @matriculationnumber 9401801
 * @email s191850@student.dhbw-mannheim.de
 *
 * Implementation of the functions from the graph
 * @param <T> The general type of the label of the vertexes
 */
public class clsGraph<T> {

    /**
     * The HashMap containing the vertexes as keys and a ArrayList of edges as values
     */
    private final Map<clsVertex<T>, ArrayList<IEdge<T>>> graph = new HashMap<>();

    /**
     * Method to add a vertex without any edges to the graph
     * @param label Label of the vertex
     */
    public void addVertex(T label){
        if(graph.putIfAbsent(new clsVertex<>(label), new ArrayList<>()) != null){
            System.out.println("There was already a vertex with this label");
        }
    }

    /**
     * Deletes the vertex with the given label and all it's incoming and outcoming edges
     * @param label Label of the vertex
     */
    public void deleteVertex(T label){
        clsVertex<T> toRemoveVertex = new clsVertex<>(label);
        graph.values().forEach(e -> e.removeIf(n -> n.getDestinationPoint().equals(toRemoveVertex)));
        graph.remove(toRemoveVertex);
    }

    /**
     * Adds a directed edge to the graph
     * @param startLabel The vertex from which the edge starts
     * @param destinationLabel The vertex to which the edge goes
     * @param weight The weight of the new edge
     */
    public void addDirectedEdge(T startLabel, T destinationLabel, double weight){
        clsVertex<T> startingVertex = new clsVertex<>(startLabel);
        clsDirectedEdge<T> newEdge = new clsDirectedEdge<>(new clsVertex<>(destinationLabel), weight);
        addEdge(startingVertex, newEdge);
    }

    /**
     * Adds a undirected edge to the graph
     * @param label1 The first vertex
     * @param label2 The second vertex
     * @param weight The weight of the new Edge
     */
    public void addUndirectedEdge(T label1, T label2, double weight){
        clsVertex<T> startingVertex = new clsVertex<>(label1);
        clsUndirectedEdge<T> newEdge = new clsUndirectedEdge<>(new clsVertex<>(label2), weight);
        addEdge(startingVertex, newEdge);

        startingVertex = new clsVertex<>(label2);
        newEdge = new clsUndirectedEdge<>(new clsVertex<>(label1), weight);
        addEdge(startingVertex, newEdge);
    }

    /**
     * Adds the given edge to the graph
     * @param startingVertex The vertex from which the edge starts
     * @param newEdge The edge which will be added to the graph
     */
    private void addEdge(clsVertex<T> startingVertex, IEdge<T> newEdge){
         graph.get(startingVertex).add(newEdge);
    }

    /**
     * Deletes a directed or undirected edge
     * @param startLabel The vertex from which the edge starts when it is a directed edge. One of the vertexes if it is a undirected edge.
     * @param destinationLabel The vertex to which the edge goes when it is a directed edge. One of the vertexes if it is a undirected edge.
     * @param weight The wight of the edge which will be deleted
     */
    public void deleteEdge(T startLabel, T destinationLabel, double weight){
        clsVertex<T> startVertex = new clsVertex<>(startLabel);
        List<IEdge<T>> edgesFromTheStartVertex = graph.get(startVertex);

        edgesFromTheStartVertex.removeIf(entry -> {
           if(entry.getDestinationPoint().getLabel() == destinationLabel && entry.getWeight() == weight){
               if(entry instanceof clsUndirectedEdge){
                   clsVertex<T> destinationVertex = new clsVertex<>(destinationLabel);
                   clsUndirectedEdge<T> toDeleteEdge = new clsUndirectedEdge<>(startVertex, weight);
                   List<IEdge<T>> edgesFromTheDestinationVertex = graph.get(destinationVertex);
                   edgesFromTheDestinationVertex.remove(toDeleteEdge);
               }
               return true;
           }
            return false;
        });
    }

    /**
     * Prints a pretty version of the graph in list form
     */
    public void printGraph() {
        graph.forEach((key, value) -> System.out.println(key.toString() + ", " + value.toString()));
    }

    /**
     * Computes all the outcoming edges to the given vertex
     * @param vertex Vertex to which the edges are searched for
     * @return A list with all the outcoming edges from the vertex
     */
    public ArrayList<IEdge<T>> getEdges(clsVertex<T> vertex){
        return graph.get(vertex);
    }

    /**
     * Computes all Vertexes which can be reached in one step from the startVertex
     * @param startVertex The vertex from which the other ones are computed
     * @return All vertexes which are directly connected to startVertex with their edges and the weight from the edge from the startVertex
     */
    public Map<clsVertex<T>, clsWrapper<T>> getReachableVertexes(clsVertex<T> startVertex){
        Map<clsVertex<T>, clsWrapper<T>> reachableVertexes = new HashMap<>();
        graph.get(startVertex).forEach(entry -> reachableVertexes.put(entry.getDestinationPoint(), new clsWrapper<>(getEdges(entry.getDestinationPoint()), entry.getWeight(), null)));
        return reachableVertexes;
    }

}
